module.exports.print = (num) => {
  let i = 1
  let str = ''
  while (i <= num) {
    if (i % 3 == 0) {
      str += 'fish'
    }
    if (i % 5 == 0) {
      str += 'bash'
    }
    i++
  }
  console.log(str)
  return str
}

module.exports.sort = (array) => {
  function mergeSort(arr, l, r) {
    if (r > l) {
      let m = Math.floor(l + ((r - l) / 2))
      mergeSort(arr, l, m)
      mergeSort(arr, m + 1, r)
      merge(arr, l, m, r)
    }
  }
  
  function merge(arr, l, m, r) {
    let l1 = m - l + 1
    let l2 = r - m
    
    let lArr = []
    let rArr = []
    
    for (let i = 0; i < l1; i++) {
      lArr[i] = arr[l + i]
    }
    for (let i = 0; i < l2; i++) {
      rArr[i] = arr[m + i + 1]
    }
    
    let i = 0
    let j = 0
    let k = l
    
    while (i < l1 && j < l2) {
      if (lArr[i] <= rArr[j]) {
        arr[k] = lArr[i]
        i++
      } else {
        arr[k] = rArr[j]
        j++
      }
      k++
    }
    
    while (i < l1) {
      arr[k] = lArr[i]
      i++
      k++
    }
    
    while (j < l2) {
      arr[k] = rArr[j]
      j++
      k++
    }
  }
  let arr = array
  mergeSort(arr, 0, arr.length - 1)
  console.log(JSON.stringify(arr))
  return arr
}

module.exports.isPalindrome = (string) => {
  let l = 0
  let r = string.length - 1
  while (l < r) {
    if (string[l] != string[r]) return false
    l++
    r--
  }
  return true
}
