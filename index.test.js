// import { expect } from "@jest/globals"
const func = require('./index.js')

test('Fish Bash', () => {
  expect(func.print(15)).toBe('fishbashfishfishbashfishfishbash')
  expect(func.print(6)).toBe('fishbashfish')
  expect(func.print(0)).toBe('')
})

test('Sort Array', () => {
  expect(func.sort([35, 49, 48, 5, 27, 3, 22, 45, 19, 10])).toStrictEqual([3,5,10,19,22,27,35,45,48,49])
  expect(func.sort([25, 38, 30, -10, -49, 40, 24])).toStrictEqual([-49,-10,24,25,30,38,40])
  expect(func.sort([0])).toStrictEqual([0])
})

test('Is Palindrome ?', () => {
  expect(func.isPalindrome('appa')).toBe(true)
  expect(func.isPalindrome('dapa')).toBe(false)
  expect(func.isPalindrome('4125214')).toBe(true)
})